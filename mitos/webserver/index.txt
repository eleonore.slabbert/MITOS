<div style="width:500px;margin:0 auto;text-align: justify;">
<form name="mtdb" action="upload.py" enctype="multipart/form-data" method="POST">
			<table border=0 style="width:500px;table-layout:fixed;"> 
					<tr title="Your name for addressing you in the notification.">
						<td style="width:100px">Name:</td>
						<td style="text-align: right;"><input type="text" name="name" style="width:250px;"> </td>
					</tr>
					<tr title="Your email address for sending the notification.">
						<td>Email:</td>
						<td style="text-align: right;"><input type="text" name="email" style="width:250px;"> </td>
					</tr>
					<tr title="An identifier for your job.">
					    <td style="width:100px">Job identifier:</td>
						<td style="text-align: right;"><input type="text" name="jobid" style="width:250px;"> </td>
					</tr>
					<tr title="The genetic code to be used in the annotation.">
						<td>Genetic Code*:</td>
						<td style="text-align: right;">
						<select name="code" size="1"  style="width:250px;">
							<option value="2">02 - Vertebrate</option>
							<option value="4">04 - Mold|Protozoan|Coelenterate</option>
							<option value="5">05 - Invertebrate</option>
							<option value="9">09 - Echinoderm|Flatworm</option>
							<option value="13">13 - Ascidian</option>
							<option value="14">14 - Alternative Flatworm</option>
						</select>
						</td>
					</tr>
					<tr title="Select the fasta file containing the sequence to be annotated.">
						<td>Fasta File*:</td>
						<td style="text-align: right;"><input type="file" name="myFile"></td>
					</tr>
					<tr><td>* = required</td>
					    <td style="text-align:right"  title="Click here to submit your job to MITOS." ><input value="" name="proceed" class="proceed" onmouseover='this.style.cursor="pointer"' type="button" onclick="button01()">&nbsp;&nbsp;</td>
					</tr>
					
					<tr><td colspan="2">A tutorial on how to use <b>MITOS</b>, including an <a href="help.py#example">example</a> and the used sample data, can be found <a href="help.py">here</a>.</td></tr>

					<tr"><td colspan="2"  title="Click to see advanced options."><input name="advanced" class="advanced" onmouseover='this.style.cursor="pointer"' type="button" id="advance" onclick ="showall()"/></td></tr>
			</table>
			<table border=0 style="width:500px;table-layout:fixed;"> 
					<tr class="hidein" title="Feature types that should be predicted by MITOS">
						<td style="width=100;">Feature types:</td>
						<td>
						<table>  
						<tr align="center">
						<td style="white-space: nowrap;">Proteins<input type="checkbox" checked="checked" id="prot" name="prot" /></td>
						<td style="white-space: nowrap;">tRNAs<input type="checkbox" checked="checked" id="trna" name="trna" /></td>
						<td style="white-space: nowrap;">rRNAs<input type="checkbox" checked="checked" id="rrna" name="rrna" /></td>
						</tr></table>
						</td>
					</tr>
					<tr class= "hidein" title="Allow upload of Multi fasta. Please see the documentation.">
						<td style="white-space: nowrap;">Allow Multifasta:</td><td><input type="checkbox" id="prot" name="multi"> </td>
					</tr>					
					<tr class= "hidein" title="Negation of the exponent of the E-value threshold used by BLAST, i.e. a value X gives an E-value of 10^(-X).">
						<td style="white-space: nowrap;">BLAST E-value Exponent:</td><td><input type="text" name="evalue" value = "2" width=50> </td>
					</tr>
					<tr class= "hidein" title="Minimum allowed quality in % of the maximum quality value per reading frame">
						<td style="white-space: nowrap;">Cutoff:</td><td><input type="text" name="cutoff" value = "50"> </td>
					</tr>
					<tr class= "hidein" title="Maximum allowed overlap of proteins in percent of the smaller feature">
						<td style="white-space: nowrap;">Maximum Overlap:</td><td><input type="text" name="maxovl" value = "20"> </td>
					</tr>
					<tr class= "hidein" title="Clip overlapping proteins with the same name that differ by less than the specified factor">
						<td style="white-space: nowrap;">Clipping Factor:</td><td><input type="text" name="clipfac" value = "10"> </td>
					</tr>
					<tr class= "hidein" title="Maximum allowed overlap of proteins in the query (in percent of the shorter query range) for two hits to be counted as fragments of the same gene">
						<td style="white-space: nowrap;">Fragment Overlap:</td><td><input type="text" name="fragovl" value = "20"> </td>
					</tr>
					<tr class= "hidein" title="Maximum factor by which fragments of the same protein may differ in their quality">
						<td style="white-space: nowrap;">Fragment Quality Factor:</td><td><input type="text" name="fragfac" value = "10"> </td>
					</tr>
					<tr class= "hidein" title="Number of aminoacids searched for start and stop codon of proteins">
						<td style="white-space: nowrap;">Start/Stop Range:</td><td><input type="text" name="ststrange" value ="6"> </td>
					</tr>
					<tr class= "hidein" title="Maximum number of nucleotides by which genes of different types may overlap">
						<td style="white-space: nowrap;">Final Maximum Overlap:</td><td><input type="text" name="finovl" value = "35"> </td>
					</tr>
			</table>
</form>

<div style="clear: both;"></div>



</div>

